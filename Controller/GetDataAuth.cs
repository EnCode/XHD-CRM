﻿/*
* GetDataAuth.cs
*
* 功 能： N/A
* 类 名： GetDataAuth
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using XHD.BLL;
using XHD.Common;

namespace XHD.Controller
{
    public class GetDataAuth
    {
        public GetDataAuth()
        {
        }

        public string GetDataAuthByid(string optionid, string option, int empid)
        {
            if (optionid == "xhd")
                return "all"; 
            
            var emp = new hr_employee();
            DataSet ds1 = emp.GetList(string.Format("id = {0}", empid));

            if (ds1.Tables[0].Rows[0]["uid"].ToString() == "admin")
                return "all";

            string RoleIDs = GetRoleidByUID(empid);
            var sda = new Sys_data_authority();
            DataSet ds = sda.GetList(string.Format(" option_id= {0} and Role_id in {1}", optionid, RoleIDs));

            int temp = 0;
            if (ds.Tables[0].Rows.Count > 0)
            {
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    if (int.Parse(ds.Tables[0].Rows[i][option].ToString()) > temp)
                        temp = int.Parse(ds.Tables[0].Rows[i][option].ToString());
                }
                //return temp.ToString();
            }

            switch (temp)
            {
                case 0:
                    return "none";
                case 1:
                    return "my:" + empid;
                case 2:
                    return "dep:" + ds1.Tables[0].Rows[0]["d_id"].ToString();
                case 3:
                    return "depall:" + ds1.Tables[0].Rows[0]["d_id"].ToString();
                case 4:
                    return "all";
            }
            return "";
        }

        private string GetRoleidByUID(int uid)
        {
            if (PageValidate.IsNumber(uid.ToString()))
            {
                var rm = new Sys_role_emp();
                DataSet ds = rm.GetList(string.Format("empID = {0}", uid));
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string RoleIDs = "(";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        RoleIDs += ds.Tables[0].Rows[i]["RoleID"].ToString() + ",";
                    }
                    RoleIDs = RoleIDs.Substring(0, RoleIDs.Length - 1);
                    RoleIDs += ")";
                    return RoleIDs;
                }
                else
                {
                    return "(0)";
                }                
            }
            return "(0)";
        }
    }
}