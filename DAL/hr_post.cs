﻿/*
* hr_post.cs
*
* 功 能： N/A
* 类 名： hr_post
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:59:28    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using XHD.DBUtility;

//Please add references

namespace XHD.DAL
{
    /// <summary>
    ///     数据访问类:hr_post
    /// </summary>
    public class hr_post
    {
        #region  BasicMethod		

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.hr_post model)
        {
            var strSql = new StringBuilder();
            strSql.Append("insert into hr_post(");
            strSql.Append("post_name,position_id,dep_id,emp_id,default_post,note,post_descript,isDelete,Delete_time)");
            strSql.Append(" values (");
            strSql.Append(
                "@post_name,@position_id,@dep_id,@emp_id,@default_post,@note,@post_descript,@isDelete,@Delete_time)");
            strSql.Append(";select @@IDENTITY");
            SqlParameter[] parameters =
            {
                new SqlParameter("@post_name", SqlDbType.VarChar, 255),
                new SqlParameter("@position_id", SqlDbType.Int, 4),
                new SqlParameter("@dep_id", SqlDbType.Int, 4),
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@default_post", SqlDbType.Int, 4),
                new SqlParameter("@note", SqlDbType.VarChar, -1),
                new SqlParameter("@post_descript", SqlDbType.VarChar, -1),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime)
            };
            parameters[0].Value = model.post_name;
            parameters[1].Value = model.position_id;
            parameters[2].Value = model.dep_id;
            parameters[3].Value = model.emp_id;
            parameters[4].Value = model.default_post;
            parameters[5].Value = model.note;
            parameters[6].Value = model.post_descript;
            parameters[7].Value = model.isDelete;
            parameters[8].Value = model.Delete_time;

            object obj = DbHelperSQL.GetSingle(strSql.ToString(), parameters);
            if (obj == null)
            {
                return 0;
            }
            return Convert.ToInt32(obj);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.hr_post model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update hr_post set ");
            strSql.Append("post_name=@post_name,");
            strSql.Append("position_id=@position_id,");
            strSql.Append("dep_id=@dep_id,");
            strSql.Append("emp_id=@emp_id,");
            strSql.Append("default_post=@default_post,");
            strSql.Append("note=@note,");
            strSql.Append("post_descript=@post_descript,");
            strSql.Append("isDelete=@isDelete,");
            strSql.Append("Delete_time=@Delete_time");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@post_name", SqlDbType.VarChar, 255),
                new SqlParameter("@position_id", SqlDbType.Int, 4),
                new SqlParameter("@dep_id", SqlDbType.Int, 4),
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@default_post", SqlDbType.Int, 4),
                new SqlParameter("@note", SqlDbType.VarChar, -1),
                new SqlParameter("@post_descript", SqlDbType.VarChar, -1),
                new SqlParameter("@isDelete", SqlDbType.Int, 4),
                new SqlParameter("@Delete_time", SqlDbType.DateTime),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = model.post_name;
            parameters[1].Value = model.position_id;
            parameters[2].Value = model.dep_id;
            parameters[3].Value = model.emp_id;
            parameters[4].Value = model.default_post;
            parameters[5].Value = model.note;
            parameters[6].Value = model.post_descript;
            parameters[7].Value = model.isDelete;
            parameters[8].Value = model.Delete_time;
            parameters[9].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from hr_post ");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@id", SqlDbType.Int, 4)
            };
            parameters[0].Value = id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     批量删除数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            var strSql = new StringBuilder();
            strSql.Append("delete from hr_post ");
            strSql.Append(" where id in (" + idlist + ")  ");
            int rows = DbHelperSQL.ExecuteSql(strSql.ToString());
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            var strSql = new StringBuilder();
            strSql.Append(
                "select id,post_name,position_id,dep_id,emp_id,default_post,note,post_descript,isDelete,Delete_time ");
            strSql.Append(
                " ,(select position_name from hr_position where id = hr_post.[position_id]) as [position_name]  ");
            strSql.Append(
                " ,(select position_order from hr_position where id = hr_post.[position_id]) as [position_order]  ");
            strSql.Append(" ,(select d_name from hr_department where id = hr_post.[dep_id]) as [depname]  ");
            strSql.Append(" ,(select name from hr_employee where id = hr_post.[emp_id]) as [emp_name]  ");
            strSql.Append(" FROM hr_post ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            var strSql = new StringBuilder();
            strSql.Append("select ");
            if (Top > 0)
            {
                strSql.Append(" top " + Top);
            }
            strSql.Append(
                " id,post_name,position_id,dep_id,emp_id,default_post,note,post_descript,isDelete,Delete_time ");
            strSql.Append(
                " ,(select position_name from hr_position where id = hr_post.[position_id]) as [position_name]  ");
            strSql.Append(
                " ,(select position_order from hr_position where id = hr_post.[position_id]) as [position_order]  ");
            strSql.Append(" ,(select d_name from hr_department where id = hr_post.[dep_id]) as [depname]  ");
            strSql.Append(" ,(select name from hr_employee where id = hr_post.[emp_id]) as [emp_name]  ");
            strSql.Append(" FROM hr_post ");
            if (strWhere.Trim() != "")
            {
                strSql.Append(" where " + strWhere);
            }
            strSql.Append(" order by " + filedOrder);
            return DbHelperSQL.Query(strSql.ToString());
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            var strSql_grid = new StringBuilder();
            var strSql_total = new StringBuilder();
            strSql_total.Append(" SELECT COUNT(id) FROM hr_post ");
            strSql_grid.Append("SELECT ");
            strSql_grid.Append(
                "      id,post_name,position_id,dep_id,emp_id,default_post,note,post_descript,isDelete,Delete_time ");
            strSql_grid.Append(
                " ,(select position_name from hr_position where id = w1.[position_id]) as [position_name]  ");
            strSql_grid.Append(
                " ,(select position_order from hr_position where id = w1.[position_id]) as [position_order]  ");
            strSql_grid.Append(" ,(select d_name from hr_department where id = w1.[dep_id]) as [depname]  ");
            strSql_grid.Append(" ,(select name from hr_employee where id = w1.[emp_id]) as [emp_name]  ");
            strSql_grid.Append(
                " FROM ( SELECT id,post_name,position_id,dep_id,emp_id,default_post,note,post_descript,isDelete,Delete_time, ROW_NUMBER() OVER( Order by " +
                filedOrder + " ) AS n from hr_post");
            if (strWhere.Trim() != "")
            {
                strSql_grid.Append(" WHERE " + strWhere);
                strSql_total.Append(" WHERE " + strWhere);
            }
            strSql_grid.Append("  ) as w1  ");
            strSql_grid.Append("WHERE n BETWEEN " + PageSize*(PageIndex - 1) + " AND " + PageSize*PageIndex);
            strSql_grid.Append(" ORDER BY " + filedOrder);
            Total = DbHelperSQL.Query(strSql_total.ToString()).Tables[0].Rows[0][0].ToString();
            return DbHelperSQL.Query(strSql_grid.ToString());
        }

        #endregion  BasicMethod

        #region  ExtensionMethod

        /// <summary>
        ///     更新岗位人员
        /// </summary>
        public bool UpdatePostEmp(Model.hr_post model)
        {
            var strSql = new StringBuilder();
            strSql.Append("update hr_post set ");
            strSql.Append("emp_id=@emp_id,");
            strSql.Append("default_post=@default_post");
            strSql.Append(" where id=@id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4),
                new SqlParameter("@default_post", SqlDbType.Int, 4),
                new SqlParameter("@id", SqlDbType.Int, 4)
            };

            parameters[0].Value = model.emp_id;
            parameters[1].Value = model.default_post;
            parameters[2].Value = model.id;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        ///     清空更新岗位人员
        /// </summary>
        public bool UpdatePostEmpbyEid(int empid)
        {
            var strSql = new StringBuilder();
            strSql.Append("update hr_post set ");

            strSql.Append("emp_id=-1,");
            strSql.Append("default_post=0");
            strSql.Append(" where emp_id=@emp_id");
            SqlParameter[] parameters =
            {
                new SqlParameter("@emp_id", SqlDbType.Int, 4)
            };

            parameters[0].Value = empid;

            int rows = DbHelperSQL.ExecuteSql(strSql.ToString(), parameters);
            if (rows > 0)
            {
                return true;
            }
            return false;
        }

        #endregion  ExtensionMethod
    }
}