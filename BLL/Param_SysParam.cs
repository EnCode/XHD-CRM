/*
* Param_SysParam.cs
*
* 功 能： N/A
* 类 名： Param_SysParam
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:53:47    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Collections.Generic;
using System.Data;
using XHD.Common;

namespace XHD.BLL
{
    /// <summary>
    ///     Param_SysParam
    /// </summary>
    public class Param_SysParam
    {
        private readonly DAL.Param_SysParam dal = new DAL.Param_SysParam();

        #region  Method

        /// <summary>
        ///     得到最大ID
        /// </summary>
        public int GetMaxId()
        {
            return dal.GetMaxId();
        }

        /// <summary>
        ///     是否存在该记录
        /// </summary>
        public bool Exists(int id)
        {
            return dal.Exists(id);
        }

        /// <summary>
        ///     增加一条数据
        /// </summary>
        public int Add(Model.Param_SysParam model)
        {
            return dal.Add(model);
        }

        /// <summary>
        ///     更新一条数据
        /// </summary>
        public bool Update(Model.Param_SysParam model)
        {
            return dal.Update(model);
        }


        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool Delete(int id)
        {
            return dal.Delete(id);
        }

        /// <summary>
        ///     删除一条数据
        /// </summary>
        public bool DeleteList(string idlist)
        {
            return dal.DeleteList(idlist);
        }

        /// <summary>
        ///     得到一个对象实体
        /// </summary>
        public Model.Param_SysParam GetModel(int id)
        {
            return dal.GetModel(id);
        }

        /// <summary>
        ///     得到一个对象实体，从缓存中
        /// </summary>
        public Model.Param_SysParam GetModelByCache(int id)
        {
            string CacheKey = "Param_SysParamModel-" + id;
            object objModel = DataCache.GetCache(CacheKey);
            if (objModel == null)
            {
                try
                {
                    objModel = dal.GetModel(id);
                    if (objModel != null)
                    {
                        int ModelCache = ConfigHelper.GetConfigInt("ModelCache");
                        DataCache.SetCache(CacheKey, objModel, DateTime.Now.AddMinutes(ModelCache), TimeSpan.Zero);
                    }
                }
                catch
                {
                }
            }
            return (Model.Param_SysParam) objModel;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetList(string strWhere)
        {
            return dal.GetList(strWhere);
        }

        /// <summary>
        ///     获得前几行数据
        /// </summary>
        public DataSet GetList(int Top, string strWhere, string filedOrder)
        {
            return dal.GetList(Top, strWhere, filedOrder);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.Param_SysParam> GetModelList(string strWhere)
        {
            DataSet ds = dal.GetList(strWhere);
            return DataTableToList(ds.Tables[0]);
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public List<Model.Param_SysParam> DataTableToList(DataTable dt)
        {
            var modelList = new List<Model.Param_SysParam>();
            int rowsCount = dt.Rows.Count;
            if (rowsCount > 0)
            {
                Model.Param_SysParam model;
                for (int n = 0; n < rowsCount; n++)
                {
                    model = new Model.Param_SysParam();
                    if (dt.Rows[n]["id"] != null && dt.Rows[n]["id"].ToString() != "")
                    {
                        model.id = int.Parse(dt.Rows[n]["id"].ToString());
                    }
                    if (dt.Rows[n]["parentid"] != null && dt.Rows[n]["parentid"].ToString() != "")
                    {
                        model.parentid = int.Parse(dt.Rows[n]["parentid"].ToString());
                    }
                    if (dt.Rows[n]["params_name"] != null && dt.Rows[n]["params_name"].ToString() != "")
                    {
                        model.params_name = dt.Rows[n]["params_name"].ToString();
                    }
                    if (dt.Rows[n]["params_order"] != null && dt.Rows[n]["params_order"].ToString() != "")
                    {
                        model.params_order = int.Parse(dt.Rows[n]["params_order"].ToString());
                    }
                    if (dt.Rows[n]["Create_id"] != null && dt.Rows[n]["Create_id"].ToString() != "")
                    {
                        model.Create_id = int.Parse(dt.Rows[n]["Create_id"].ToString());
                    }
                    if (dt.Rows[n]["Create_date"] != null && dt.Rows[n]["Create_date"].ToString() != "")
                    {
                        model.Create_date = DateTime.Parse(dt.Rows[n]["Create_date"].ToString());
                    }
                    if (dt.Rows[n]["Update_id"] != null && dt.Rows[n]["Update_id"].ToString() != "")
                    {
                        model.Update_id = int.Parse(dt.Rows[n]["Update_id"].ToString());
                    }
                    if (dt.Rows[n]["Update_date"] != null && dt.Rows[n]["Update_date"].ToString() != "")
                    {
                        model.Update_date = DateTime.Parse(dt.Rows[n]["Update_date"].ToString());
                    }
                    modelList.Add(model);
                }
            }
            return modelList;
        }

        /// <summary>
        ///     获得数据列表
        /// </summary>
        public DataSet GetAllList()
        {
            return GetList("");
        }

        /// <summary>
        ///     分页获取数据列表
        /// </summary>
        public DataSet GetList(int PageSize, int PageIndex, string strWhere, string filedOrder, out string Total)
        {
            return dal.GetList(PageSize, PageIndex, strWhere, filedOrder, out Total);
        }

        #endregion  Method
    }
}