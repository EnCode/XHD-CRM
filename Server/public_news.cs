﻿/*
* public_news.cs
*
* 功 能： N/A
* 类 名： public_news
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;
using System.Data;
using System.Web;
using XHD.Common;
using XHD.Controller;

namespace XHD.Server
{
    public class public_news
    {
        public static BLL.public_news news = new BLL.public_news();
        public static Model.public_news model = new Model.public_news();

        public HttpContext Context;
        public int emp_id;
        public string emp_name;
        public Model.hr_employee employee;
        public HttpRequest request;
        public string uid;


        public public_news()
        {
        }

        public public_news(HttpContext context)
        {
            Context = context;
            request = context.Request;

            var userinfo = new User_info();
            employee = userinfo.GetCurrentEmpInfo(context);

            emp_id = employee.ID;
            emp_name = PageValidate.InputText(employee.name, 50);
            uid = PageValidate.InputText(employee.uid, 50);
        }

        public void save()
        {
            model.news_time = DateTime.Now;
            model.news_title = PageValidate.InputText(request["T_title"], 255);
            model.news_content = PageValidate.InputText(request["T_content"], int.MaxValue);

            string nid = PageValidate.InputText(request["nid"], 50);
            if (PageValidate.IsNumber(nid))
            {
                DataSet ds = news.GetList("id=" + int.Parse(nid));
                DataRow dr = ds.Tables[0].Rows[0];

                model.id = int.Parse(nid);

                news.Update(model);

                var log = new sys_log();

                int UserID = emp_id;
                string UserName = emp_name;
                string IPStreet = request.UserHostAddress;
                string EventTitle = model.news_title;
                string EventType = "新闻修改";
                int EventID = model.id;
                string Log_Content = null;

                if (dr["news_title"].ToString() != request["T_title"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "新闻标题", dr["news_title"], request["T_title"]);

                if (dr["news_content"].ToString() != request["T_content"])
                    Log_Content += string.Format("【{0}】{1} → {2} \n", "新闻内容", "原内容被修改", "原内容被修改");

                if (!string.IsNullOrEmpty(Log_Content))
                    log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, Log_Content);
            }
            else
            {
                model.dep_id = employee.d_id;
                model.create_id = emp_id;

                news.Add(model);
            }
        }

        public string grid()
        {
            int PageIndex = int.Parse(request["page"] == null ? "1" : request["page"]);
            int PageSize = int.Parse(request["pagesize"] == null ? "30" : request["pagesize"]);
            string sortname = request["sortname"];
            string sortorder = request["sortorder"];

            if (string.IsNullOrEmpty(sortname))
                sortname = " news_time";
            if (string.IsNullOrEmpty(sortorder))
                sortorder = "desc";

            string sorttext = " " + sortname + " " + sortorder;

            string Total, serchtxt = "1=1 ";

            if (!string.IsNullOrEmpty(request["sstart"]))
                serchtxt += " and news_time >= '" + PageValidate.InputText(request["sstart"], 50) + "'";

            if (!string.IsNullOrEmpty(request["sdend"]))
            {
                DateTime enddate = DateTime.Parse(request["sdend"]).AddHours(23).AddMinutes(59).AddSeconds(59);
                serchtxt += " and news_time  <= '" + enddate + "'";
            }

            if (!string.IsNullOrEmpty(request["stext"]))
            {
                if (request["stext"] != "输入关键词搜索")
                    serchtxt += " and news_title like N'%" + PageValidate.InputText(request["stext"], 500) + "%'";
            }

            DataSet ds = news.GetList(PageSize, PageIndex, serchtxt, sorttext, out Total);

            return (GetGridJSON.DataTableToJSON1(ds.Tables[0], Total));
        }

        public string form(int id)
        {
            DataSet ds = news.GetList("id=" + id);
            string dt = DataToJson.DataToJSON(ds);

            return dt;
        }

        //del
        public string del(int id)
        {
            bool canDel = false;

            if (uid == "admin")
            {
                canDel = true;
            }
            else
            {
                var getauth = new GetAuthorityByUid();
                canDel = getauth.GetBtnAuthority(emp_id.ToString(), "10");
                if (!canDel)
                    return ("auth");
            }

            if (canDel)
            {
                DataSet ds = news.GetList("id=" + id);

                string EventType = "新闻删除";

                bool isdel = news.Delete(id);
                if (isdel)
                {
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        int UserID = emp_id;
                        string UserName = emp_name;
                        string IPStreet = request.UserHostAddress;
                        int EventID = id;
                        string EventTitle = ds.Tables[0].Rows[i]["news_title"].ToString();

                        var log = new sys_log();
                        log.Add_log(UserID, UserName, IPStreet, EventTitle, EventType, EventID, null);
                    }
                    return ("true");
                }
                return ("false");
            }
            return ("auth");
        }

        public string newsremind()
        {
            DataSet ds = news.GetList(7, " ", " news_time desc");
            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }
    }
}