﻿/*
* Reports_CRM.cs
*
* 功 能： N/A
* 类 名： Reports_CRM
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System.Data;
using System.Web;
using XHD.Common;

namespace XHD.Server
{
    public class Reports_CRM
    {
        public HttpContext Context;
        public HttpRequest request;

        public Reports_CRM()
        {
        }

        public Reports_CRM(HttpContext context)
        {
            Context = context;
            request = context.Request;
        }

        public string CRM_Reports_year(string stype_val, int syear)
        {
            var ccc = new BLL.CRM_Customer();

            stype_val = PageValidate.InputText(stype_val, 255);

            DataSet ds = ccc.Reports_year(stype_val, syear, "");

            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }

        public string Follow_Reports_year(int syear)
        {
            var follow = new BLL.CRM_Follow();

            string items = "Follow_Type";

            DataSet ds = follow.Reports_year(items, syear, "");

            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);
            return dt;
        }

        public string Funnel(string stype_val, int syear)
        {
            string whereStr = "";

            stype_val = PageValidate.InputText(stype_val, 255);
            if (!string.IsNullOrEmpty(stype_val) && stype_val != "null")
                whereStr = string.Format("  a.id in({0})", stype_val.Replace(';', ','));

            //context.Response.Write(whereStr);

            var ccc = new BLL.CRM_Customer();
            DataSet ds = ccc.Funnel(whereStr, syear.ToString());

            string dt = GetGridJSON.DataTableToJSON(ds.Tables[0]);

            return dt;
        }
    }
}