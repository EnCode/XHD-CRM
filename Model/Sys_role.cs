/*
* Sys_role.cs
*
* 功 能： N/A
* 类 名： Sys_role
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-23 18:38:21    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     Sys_role:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class Sys_role
    {
        #region Model

        private DateTime? _createdate;
        private int? _createid;
        private string _roledscript;
        private int _roleid;
        private string _rolename;
        private int? _rolesort;
        private DateTime? _updatedate;
        private int? _updateid;

        /// <summary>
        /// </summary>
        public int RoleID
        {
            set { _roleid = value; }
            get { return _roleid; }
        }

        /// <summary>
        /// </summary>
        public string RoleName
        {
            set { _rolename = value; }
            get { return _rolename; }
        }

        /// <summary>
        /// </summary>
        public string RoleDscript
        {
            set { _roledscript = value; }
            get { return _roledscript; }
        }

        /// <summary>
        /// </summary>
        public int? RoleSort
        {
            set { _rolesort = value; }
            get { return _rolesort; }
        }

        /// <summary>
        /// </summary>
        public int? CreateID
        {
            set { _createid = value; }
            get { return _createid; }
        }

        /// <summary>
        /// </summary>
        public DateTime? CreateDate
        {
            set { _createdate = value; }
            get { return _createdate; }
        }

        /// <summary>
        /// </summary>
        public int? UpdateID
        {
            set { _updateid = value; }
            get { return _updateid; }
        }

        /// <summary>
        /// </summary>
        public DateTime? UpdateDate
        {
            set { _updatedate = value; }
            get { return _updatedate; }
        }

        #endregion Model
    }
}