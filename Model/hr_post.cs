﻿/*
* hr_post.cs
*
* 功 能： N/A
* 类 名： hr_post
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-06-24 10:59:28    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com   All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：黄润伟                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/

using System;

namespace XHD.Model
{
    /// <summary>
    ///     hr_post:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public class hr_post
    {
        #region Model

        private int? _default_post;
        private DateTime? _delete_time;
        private int? _dep_id;
        private int? _emp_id;
        private int _id;
        private int? _isdelete;
        private string _note;
        private int? _position_id;
        private string _post_descript;
        private string _post_name;

        /// <summary>
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }

        /// <summary>
        /// </summary>
        public string post_name
        {
            set { _post_name = value; }
            get { return _post_name; }
        }

        /// <summary>
        /// </summary>
        public int? position_id
        {
            set { _position_id = value; }
            get { return _position_id; }
        }

        /// <summary>
        /// </summary>
        public int? dep_id
        {
            set { _dep_id = value; }
            get { return _dep_id; }
        }

        /// <summary>
        /// </summary>
        public int? emp_id
        {
            set { _emp_id = value; }
            get { return _emp_id; }
        }

        /// <summary>
        /// </summary>
        public int? default_post
        {
            set { _default_post = value; }
            get { return _default_post; }
        }

        /// <summary>
        /// </summary>
        public string note
        {
            set { _note = value; }
            get { return _note; }
        }

        /// <summary>
        /// </summary>
        public string post_descript
        {
            set { _post_descript = value; }
            get { return _post_descript; }
        }

        /// <summary>
        /// </summary>
        public int? isDelete
        {
            set { _isdelete = value; }
            get { return _isdelete; }
        }

        /// <summary>
        /// </summary>
        public DateTime? Delete_time
        {
            set { _delete_time = value; }
            get { return _delete_time; }
        }

        #endregion Model
    }
}