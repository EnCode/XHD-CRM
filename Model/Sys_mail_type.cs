﻿/*
* Sys_mail_type.cs
*
* 功 能： N/A
* 类 名： Sys_mail_type
*
* Ver    变更日期             负责人     变更内容
* ───────────────────────────────────
* V1.0  2015-07-20 09:50:37    黄润伟    
*
* Copyright (c) 2015 www.xhdcrm.com  www.xhdoa.com. All rights reserved.
*┌──────────────────────────────────┐
*│　版权所有：小黄豆                      　　　　　　　　　　　　　　│
*└──────────────────────────────────┘
*/
using System;
namespace XHD.Model
{
    /// <summary>
    /// Sys_mail_type:实体类(属性说明自动提取数据库字段的描述信息)
    /// </summary>
    [Serializable]
    public partial class Sys_mail_type
    {
        public Sys_mail_type()
        { }
        #region Model
        private int _id;
        private string _mail_type;
        private int? _use_ssl;
        private string _pop3;
        private int? _pop3_port;
        private string _imap;
        private int? _imap_port;
        private string _smtp;
        private int? _smtp_port;
        private int? _mail_order;
        /// <summary>
        /// 
        /// </summary>
        public int id
        {
            set { _id = value; }
            get { return _id; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string mail_type
        {
            set { _mail_type = value; }
            get { return _mail_type; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? use_ssl
        {
            set { _use_ssl = value; }
            get { return _use_ssl; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string pop3
        {
            set { _pop3 = value; }
            get { return _pop3; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? pop3_port
        {
            set { _pop3_port = value; }
            get { return _pop3_port; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string imap
        {
            set { _imap = value; }
            get { return _imap; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? imap_port
        {
            set { _imap_port = value; }
            get { return _imap_port; }
        }
        /// <summary>
        /// 
        /// </summary>
        public string smtp
        {
            set { _smtp = value; }
            get { return _smtp; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? smtp_port
        {
            set { _smtp_port = value; }
            get { return _smtp_port; }
        }
        /// <summary>
        /// 
        /// </summary>
        public int? mail_order
        {
            set { _mail_order = value; }
            get { return _mail_order; }
        }
        #endregion Model

    }
}

