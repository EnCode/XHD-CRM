﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;


namespace XHD.View
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            BLL.Param_SysParam pa = new BLL.Param_SysParam();
            DataSet ds = pa.GetAllList();

            Dictionary<int, String> dic = new Dictionary<int, String>();
            for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
            {
                dic.Add(int.Parse(ds.Tables[0].Rows[i]["id"].ToString()), ds.Tables[0].Rows[i]["params_name"].ToString());
            }
            string a;
            if (dic.TryGetValue(65, out a))
            {
                Response.Write(a);
            }
            else
                Response.Write("false");
        }
    }
}