<%@ Page Language="C#" AutoEventWireup="true" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <meta http-equiv="X-UA-Compatible" content="ie=8 chrome=1" />
    <link href="../../CSS/input.css" rel="stylesheet" />
    <link href="../../lib/ligerUI/skins/ext/css/ligerui-all.css" rel="stylesheet" type="text/css" />

    <script src="../../lib/jquery/jquery-1.3.2.min.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerForm.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerComboBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerRadio.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerSpinner.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTextBox.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerDateEditor.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerCheckBox.js" type="text/javascript"></script>

    <script src="../../lib/ligerUI/js/plugins/ligerDialog.js" type="text/javascript"></script>

    <script src="../../lib/jquery-validation/jquery.validate.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/jquery.metadata.js" type="text/javascript"></script>
    <script src="../../lib/jquery-validation/messages_cn.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/common.js" type="text/javascript"></script>
    <script src="../../lib/ligerUI/js/plugins/ligerTip.js" type="text/javascript"></script>
    <script src="../../lib/jquery.form.js" type="text/javascript"></script>
    <script src="../../JS/Toolbar.js" type="text/javascript"></script>
    <script src="../../JS/XHD.js" type="text/javascript"></script>
    <script type="text/javascript">
        $(function () {
            $.metadata.setType("attr", "validate");
            XHD.validate($(form1));

            $("form").ligerForm();           
    
            loadForm(getparastr("fid"));

            $('#T_contact').ligerComboBox({
                width: 196,
                onBeforeOpen: f_selectContact
            });
        })
       
        function f_save() {
            if ($(form1).valid()) {
                var sendtxt = "&fid=" + getparastr("fid") + "&cid=" + getparastr("cid");
                return $("form :input").fieldSerialize() + sendtxt;
            }
        }

        function loadForm(oaid) {
            $.ajax({
                type: "GET",
                url: "CRM_Follow.form.xhd", /* 注意后面的名字对应CS的方法名称 */
                data: { id: oaid, rnd: Math.random() }, /* 注意参数的格式和名称 */
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (result) {
                    var obj = eval(result);
                    for (var n in obj) {
                        if (obj[n] == "null" || obj[n] == null)
                            obj[n] = "";
                    }
                    //alert(obj.constructor); //String 构造函数
                    $("#T_follow").val(obj.Follow);
                    $("#T_followtype").ligerComboBox({ width: 196, initValue: obj.Follow_Type_id, url: "Param_SysParam.combo.xhd?parentid=4&rnd=" + Math.random() });
                    $("#T_followaim").ligerComboBox({ width: 196, initValue: obj.Follow_aim_id, url: "Param_SysParam.combo.xhd?parentid=9&rnd=" + Math.random() });
                    $("#T_contact_val").val(obj.Contact_id);
                    $("#T_contact").val(obj.Contact);
                }
            });

        }
        function f_selectContact() {
            top.$.ligerDialog.open({
                zindex: 9005, title: '选择联系人', width: 650, height: 300, url: 'crm/customer/getContact.aspx?customerid=' + getparastr('cid'), buttons: [
                    { text: '确定', onclick: f_selectContactOK },
                    { text: '取消', onclick: f_selectContactCancel }
                ]
            });
            return false;
        }
        function f_selectContactOK(item, dialog) {
            var data = dialog.frame.f_select();
            if (!data) {
                alert('请选择行!');
                return;
            }
            $("#T_contact").val(data.C_name);
            $("#T_contact_val").val(data.id);
            dialog.close();
        }
        function f_selectContactCancel(item, dialog) {
            dialog.close();
        }
    </script>
</head>
<body>
    <form id="form1" onsubmit="return false">
        <table class="bodytable0" style="width: 597px; margin: 2px;">
            <tr>
                <td class="table_title1" colspan="2">跟进</td>
            </tr>
            <tr>

                <td class="table_label">联系人：</td>
                <td>                   
                    <input type="text" id="T_contact" name="T_contact" validate="{required:true}" />

                </td>
            </tr>
            <tr>

                <td class="table_label">跟进目的：</td>
                <td>
                    <input type="text" id="T_followaim" name="T_followaim" validate="{required:true}" /></td>
            </tr>
            <tr>

                <td class="table_label"> 跟进方式：
                </td>
                <td>
                    <input type="text" id="T_followtype" name="T_followtype" validate="{required:true}" /></td>
            </tr>
            <tr>
                <td class="table_label"> 跟进内容：</td>
                <td>
                    <textarea id="T_follow" cols="6" name="T_follow" class="l-textarea" style="width: 440px;" rows="10" validate="{required:true}"></textarea></td>

            </tr>
            
        </table>



    </form>
</body>
</html>
